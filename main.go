package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/exec"
	"os/signal"
	"strconv"
	"time"
	"unicode"

	"github.com/joho/godotenv"
)

func executeBashCommand() string {
	cmd := exec.Command("vcgencmd", "measure_temp")
	stdout, err := cmd.Output()
	if err != nil {
		fmt.Println(err.Error())
		return ""
	}
	return string(stdout)
}

func searchGradius(s string) string {
	gradius := ""
	for _, c := range s {
		if unicode.IsDigit(c) {
			gradius = gradius + string(c)
		} else if c == '.' {
			gradius = gradius + string(c)
		}
	}
	return gradius
}

func setSigIntHandler() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		for range c {
			// sig is ^C
			os.Exit(0)
		}
	}()
}

func sendTelegramNotification(text string, bot string, chat_id string) (err error) {
	request_url := "https://api.telegram.org/" + bot + "/sendMessage"

	client := &http.Client{}

	values := map[string]string{"text": text, "chat_id": chat_id}
	json_paramaters, err := json.Marshal(values)
	if err != nil {
		return err
	}

	req, err := http.NewRequest("POST", request_url, bytes.NewBuffer(json_paramaters))
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/json")

	res, err := client.Do(req)
	if err != nil {
		return err
	}
	if res.Status != "200 OK" {
		return errors.New(res.Status)
	}

	return nil
}

type HotTimer struct {
	isTooHot    bool
	timer       int64
	messageSent bool
}

func (hotTimer *HotTimer) SwitchStatus() {
	hotTimer.isTooHot = !hotTimer.isTooHot
	hotTimer.timer = time.Now().Unix()
	hotTimer.messageSent = false
}

func (hotTimer *HotTimer) IsStressed() bool {
	if hotTimer.isTooHot && time.Now().Unix() > hotTimer.timer+1*60 {
		return true
	}
	return false
}

func (hotTimer *HotTimer) StatusSwitchNotified() {
	hotTimer.messageSent = true
}

func (hotTimer *HotTimer) IsStatusSwitchNotified() bool {
	return hotTimer.messageSent
}

func NewTimer() *HotTimer {
	return &HotTimer{false, 0, false}
}

func main() {
	setSigIntHandler()

	hotTimer := NewTimer()

	hostname, err := os.Hostname()
	if err != nil {
		fmt.Println(err)
		hostname = " (error) " // for debug
	}

	chatId := os.Getenv("TELEGRAM_CHAT_ID")
	botToken := os.Getenv("TELEGRAM_BOT_TOKEN")
	if len(chatId) == 0 && len(botToken) == 0 {
		// look for .env file
		err = godotenv.Load()
		if err != nil {
			fmt.Println(err)
			fmt.Println("create .env file")
			fmt.Println("needed key: " + "TELEGRAM_CHAT_ID")
			fmt.Println("needed key: " + "TELEGRAM_BOT_TOKEN")
			os.Exit(1)
		}
		chatId = os.Getenv("TELEGRAM_CHAT_ID")
		botToken = os.Getenv("TELEGRAM_BOT_TOKEN")
	}

	for {
		text := executeBashCommand()
		// text := "temp=85.1'C"
		value := searchGradius(text)
		gradius, err := strconv.ParseFloat(value, 64)
		if err != nil {
			fmt.Println(err)
			return
		}
		if gradius-80.0 >= 0 {
			if !hotTimer.isTooHot {
				hotTimer.SwitchStatus()
			}
			if hotTimer.isTooHot {
				if hotTimer.IsStressed() && !hotTimer.IsStatusSwitchNotified() {
					msg := hostname + ": is too HOT | " + value
					err = sendTelegramNotification(msg, "bot"+botToken, chatId)
					if err != nil {
						log.Printf("Telegram notification sending error: %s", err)
					} else {
						hotTimer.StatusSwitchNotified()
					}
				}
			}
		} else {
			hotAlertWasSent := false
			if hotTimer.isTooHot {
				hotAlertWasSent = hotTimer.IsStatusSwitchNotified()
				hotTimer.SwitchStatus()
			}
			if hotAlertWasSent && !hotTimer.IsStatusSwitchNotified() {
				msg := hostname + ": is not too hot anymore | " + value
				err = sendTelegramNotification(msg, "bot"+botToken, chatId)
				if err != nil {
					log.Printf("Telegram notification sending error: %s", err)
				} else {
					hotTimer.StatusSwitchNotified()
				}
			}
		}
		time.Sleep(5 * time.Second)
	}
}
