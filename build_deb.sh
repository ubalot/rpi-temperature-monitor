#!/usr/bin/env bash

set -e

package=$(grep Package < rpi-temperature-monitor/DEBIAN/control | cut -d ' ' -f 2)
version=$(grep Version < rpi-temperature-monitor/DEBIAN/control | cut -d ' ' -f 2)
architecture=$(grep Architecture < rpi-temperature-monitor/DEBIAN/control | cut -d ' ' -f 2)

# compile the program
env GOOS=linux GOARCH=arm GOARM=5 go build

mkdir -p rpi-temperature-monitor/usr/local/bin/
cp rpi_temperature_monitor rpi-temperature-monitor/usr/local/bin/rpi_temperature_monitor
dpkg-deb --build rpi-temperature-monitor
mv rpi-temperature-monitor.deb "${package}_${version}_${architecture}".deb