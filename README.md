# RPI temperature monitor

## Production (Install as .deb)

### Install as Systemd service

1. **Build the .deb package**:
```bash
./build_deb.h
```

2. **Install the .deb package**
```bash
sudo dpkg -i rpi-temperature-monitor_1.0_arm64.deb
```

### Edit .env file to receive telegram notification

Content of `/etc/rpi-temperature.env`
```
TELEGRAM_CHAT_ID=XXXXXXXX
TELEGRAM_BOT_TOKEN=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
```
Note: Replace XXXXXXXX with your Telegram chat ID and XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX with your Telegram bot token.

---
---

## Develop

### Build (not needed for normal development)
```bash
env GOOS=linux GOARCH=arm GOARM=5 go build
```

### Run (no installation needed)
```bash
./rpi_temperature_monitor
```

Alternatively, run it inside a screen session to keep it running in the background:
```bash
screen -dmS rpi_temperature_monitor ./rpi_temperature_monitor
```

For telegram notification create a `.env` file in prject root directory, here an example:
```
TELEGRAM_CHAT_ID=XXXXXXXX
TELEGRAM_BOT_TOKEN=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
```

### Source code static analysis
If you use docker + vscode search for `vet` action, otherwise run
```bash
go vet
```

### Code format
If you use docker + vscode search for `format` action, otherwise run
```bash
go fmt .
```

### Update dependencies
```bash
go get -u
go mod tidy
```

### Docker

#### Build docker image
```bash
docker build --tag rpi-monitor .
```

#### Run docker container
```bash
docker run --rm -it \
    --name rpi-temp-monitor \
    --workdir /app \
    rpi-monitor
```

---
---

Additional Notes:
* Telegram Setup: Make sure you set up a Telegram bot correctly. If you don't have a bot yet, follow [this guide](https://core.telegram.org/bots/tutorial).
* Docker Usage: Running in Docker ensures that the application works consistently across different environments. You can also use Docker Compose for easier orchestration in multi-container setups.