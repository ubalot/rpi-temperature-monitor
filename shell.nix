let
  pkgs = import <nixpkgs> {};
in pkgs.mkShell {
  packages = [
    (with pkgs; [
      nixpkgs-fmt
      #rnix-lsp
      docker-client
      gnumake
      dpkg

      # go development
      go
      go-outline
      gopls
      gopkgs
      go-tools
      delve
    ])
  ];
}
